import org.junit.After;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.w3c.dom.stylesheets.LinkStyle;

import java.util.ArrayList;
import java.util.List;

import static org.junit.Assert.assertEquals;

public class LotteryTest {

    private Lottery lottery;
    private ArrayList<String> arrayList = new ArrayList<String>();

    @Before
    public void setup(){
        lottery = new Lottery(0, 999999);
        arrayList.add("000000");
        arrayList.add("111111");
        arrayList.add("555555");
    }

    @Test(timeout = 3000, expected = NumberFormatException.class)
    public void testSpeed(){
        lottery.calculate();
    }

    @Test
    public void testEasyMethod(){
        assertEquals(3, lottery.easyMethod(arrayList));
    }

    @Test
    public void testHardMethod(){
        assertEquals(3, lottery.hardMethod(arrayList));
    }


    @After
    public void clean(){
        arrayList.clear();
    }


}
