import java.util.Scanner;

public class Main {
    public static void main(String[] args) {
        System.out.println("Добро пожаловать в \"Счастливые билеты!\"");
        System.out.println("                      \n");

        Scanner scanner = new Scanner(System.in);
        int startNumber, endNumber;

        do{
            System.out.print("Укажите минимальный номер билета: ");
            startNumber = countReader(scanner);

            System.out.print("Укажите максимальный номер билета: ");
            endNumber = countReader(scanner);

            if(startNumber >= endNumber){
                System.out.println("Ошибка! Максимальное число билета должно быть больше минимального!");
            }
        } while (startNumber >= endNumber);
        Lottery mLottery = new Lottery(startNumber, endNumber);
        mLottery.calculate();
    }

    private static int countReader(Scanner scanner) {
        int selectedNumber = 0;
        boolean isError = true;

        do {
            String numberString = scanner.nextLine();
            try {
                selectedNumber = Integer.parseInt(numberString);
                if(selectedNumber < 0) {
                    System.out.print("Число не должно быть отрицательным, повторите: ");
                } else if(numberString.length() != 6){
                    System.out.print("Число должно быть длиной 6 символов, повторите: ");
                } else {
                    isError = false;
                }
            } catch(NumberFormatException ex) {
                System.out.println("Ошибка, введите обычное число без знаков: ");
            }
        } while (isError);
        return selectedNumber;
    }
}
