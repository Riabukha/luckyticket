import java.util.ArrayList;

public class Lottery {
    private int startNumber, endNumber;
    Lottery(int startNumber, int endNumber) {
        this.startNumber = startNumber;
        this.endNumber = endNumber;
    }
    void calculate() {
        ArrayList<String>tickets = generateTickets();
        int countLuckyTicketsEasy = easyMethod(tickets);
        int countLuckyTicketsHard = hardMethod(tickets);

        System.out.println("Количество счастливых билетов в простом методе: " + countLuckyTicketsEasy);
        System.out.println("Количество счастливых билетов в сложном методе: " + countLuckyTicketsHard);

        if (countLuckyTicketsEasy > countLuckyTicketsHard) {
            System.out.println("Простой метод собрал больше счастливых билетов!");
        } else if (countLuckyTicketsEasy < countLuckyTicketsHard){
            System.out.println("Сложный метод собрал больше счастливых билетов!");
        } else {
            System.out.println("Методы собрали поровну счастливых билетов!");
        }
    }
    private ArrayList<String>generateTickets() {
        ArrayList<String>listTickets = new ArrayList<String>();
        for(int i = startNumber; i <=endNumber; i++) {
            if(i < 100000) {
                String numberString = String.valueOf(i);
                String completeNumber  = "";
                for(int j = 0; j < 6 - numberString.length(); j++) {
                    completeNumber += "0";
                }
                completeNumber += numberString;
                listTickets.add(completeNumber);
            } else {
                String completeNumber = String.valueOf(i);
                listTickets.add(completeNumber);
            }
        }
        return listTickets;
    }
    int easyMethod(ArrayList<String> tickets) {
        int countTicket = 0;

        for(String ticket: tickets) {
            String[] numbers = ticket.split("");

            int firstHalfSum = 0;
            int secondHalfSum = 0;

            for(int j = 0; j < numbers.length/2; j++) {
                firstHalfSum += Integer.parseInt(numbers[j]);
            }

            for(int j = numbers.length/2; j < numbers.length; j++) {
                secondHalfSum += Integer.parseInt(numbers[j]);
            }
            if(firstHalfSum == secondHalfSum) {
                countTicket++;
            }
        }
        return countTicket;
    }

    int hardMethod(ArrayList<String>tickets) {
        int countTicket = 0;

        for(String ticket: tickets) {
            String[] numbers = ticket.split("");

            int evenSum = 0;
            int oddSum = 0;

            for(int j = 0; j < numbers.length; j += 2) {
                evenSum += Integer.parseInt(numbers[j]);
            }

            for(int j = 1; j < numbers.length; j += 2) {
                oddSum += Integer.parseInt(numbers[j]);
            }
            if(evenSum == oddSum) {
                countTicket++;
            }
        }
        return countTicket;
    }

}
